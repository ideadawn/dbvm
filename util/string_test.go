package util

import (
	"testing"
)

func Test_StrInArr(t *testing.T) {
	str := `1`
	arr := []string{`2`}
	_ = StrInArr(str, arr)
	arr = append(arr, str)
	_ = StrInArr(str, arr)
}
