package util

// StrInArr 检查数组中是否包含指定的字符串
func StrInArr(str string, arr []string) bool {
	for i := range arr {
		if arr[i] == str {
			return true
		}
	}
	return false
}
