package util

import (
	"errors"
)

var ErrArgsNotEnough = errors.New(`arguments not enough`)
