package mysql

import (
	"errors"
)

var (
	errConnection    = errors.New(`database was not connected`)
	errTableName     = errors.New(`table name should be validated by "^[a-z0-9_]+$"`)
	errTableNotInit  = errors.New(`log table was not initiated`)
	errDeployNothing = errors.New(`there is nothing to deploy`)

	errSyntaxError = errors.New(`syntax error, Multi bytes charactor in sql`)
	// errNoNotNull  = errors.New(`"NOT NULL" is necessary`)
	// errNoDefault  = errors.New(`"DEFAULT" is necessary`)

	// errSqlNoEnd = errors.New(`sql statement without end`)

	errCreateTableINE = errors.New(`"CREATE TABLE" must follow " IF NOT EXISTS"`)
	errDropTableIE    = errors.New(`"DROP TABLE" must follow " IF EXISTS"`)
	errAlterUnknown   = errors.New(`unknown sub command for alter`)
)
