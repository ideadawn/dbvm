package dblib

import (
	"gitee.com/ideadawn/dbvm/manager"
	_ "gitee.com/ideadawn/dbvm/mysql"
	"gitee.com/ideadawn/dbvm/util"
)

// 部署
func Deploy(uri, dir, to string, forces []string) error {
	if dir == `` || uri == `` || to == `` {
		return util.ErrArgsNotEnough
	}

	mgr, err := manager.New(dir, uri)
	if err != nil {
		return err
	}

	return mgr.Deploy(to, forces)
}
