package main

import (
	"errors"

	"gitee.com/ideadawn/dbvm/util"
)

var (
	errArgsNotEnough = util.ErrArgsNotEnough
	errArgInvalid    = errors.New(`argument validate failed`)

	errCmdNotFound = errors.New(`command not found`)

	errDirCheckFailed = errors.New(`dir check failed`)

	errRequireNotFound = errors.New(`required deployment not found`)
)
