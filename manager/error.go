package manager

import (
	"errors"
)

var (
	errProjectExists  = errors.New(`project has exists`)
	errProjectNotInit = errors.New(`project not initiated`)
)
