package manager

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type myEngine struct {
	logs []*Log
}

func (m *myEngine) Connect(*Params) error {
	return nil
}
func (m *myEngine) Close() {}
func (m *myEngine) Initiate(string) error {
	return nil
}
func (m *myEngine) ListLogs() ([]*Log, error) {
	return m.logs, nil
}
func (m *myEngine) Deploy(*Plan) error {
	return nil
}
func (m *myEngine) Revert(*Plan) error {
	return nil
}

func Test_Manager(t *testing.T) {
	tmpEngine := &myEngine{
		logs: []*Log{
			{
				Name: `v1.6.0`,
			},
			{
				Name: `v1.0.7.0`,
			},
		},
	}
	RegisterEngine(`mysql`, tmpEngine)

	mgr, err := New(`../testdata`, `db:mysql://root:qwe123@127.0.0.1:3306/test?charset=utf8mb4`)
	assert.Equal(t, nil, err)

	_ = mgr.GetLogsTable()
	err = mgr.Deploy(`latest`, []string{`latest`})
	assert.Equal(t, nil, err)

	err = mgr.Deploy(`latest`, []string{``})
	assert.Equal(t, nil, err)

	err = mgr.Revert(`v1.0.7.0`)
	assert.Equal(t, nil, err)

	mgr.Close()
}
