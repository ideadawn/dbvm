package manager

import (
	"testing"

	"github.com/agiledragon/gomonkey/v2"
	"github.com/stretchr/testify/assert"
)

func Test_DbUri(t *testing.T) {
	uri := `db:sqlite:`
	_, err := ParseDbUri(uri)
	if err == nil {
		t.Fatal(uri)
	}

	_, err = ParseDbUri(`db:sqlite:/var/sqlite/my.db`)
	assert.Equal(t, nil, err)
	params, err := ParseDbUri(`db:mysql://root:qwe:123@127.0.0.1:3306/test?charset=utf8mb4_bin`)
	assert.Equal(t, nil, err)
	assert.Equal(t, `qwe:123`, params.Password)
	assert.Equal(t, `127.0.0.1`, params.Host)
	assert.Equal(t, `3306`, params.Port)
	params, err = ParseDbUri(`db:mysql://root:123@456@127.0.0.1:3306/test?charset=utf8mb4_bin#hash`)
	assert.Equal(t, nil, err)
	assert.Equal(t, `123@456`, params.Password)
	DbUri2Dsn(params)

	params, err = ParseDbUri(`db:mysql://root@127.0.0.1:3306/test#hash`)
	assert.Equal(t, nil, err)
	assert.Equal(t, ``, params.Password)
	params, err = ParseDbUri(`db:mysql:///test`)
	assert.Equal(t, nil, err)
	assert.Equal(t, ``, params.Username)
	assert.Equal(t, `test`, params.Database)
	assert.Equal(t, ``, params.Host)

	// ipv6
	params, err = ParseDbUri(`db:mysql://root:qwe123@[fc99:1176::a02:b024]:3306/test`)
	assert.Equal(t, nil, err)
	assert.Equal(t, `[fc99:1176::a02:b024]`, params.Host)
	assert.Equal(t, `3306`, params.Port)
}

func Test_ConvertUri2Dsn(t *testing.T) {
	var (
		dbUri = "db:mysql://"

		errParse = errDbUri
	)

	p := gomonkey.ApplyFunc(
		ParseDbUri,
		func(_ string) (*Params, error) {
			return nil, errParse
		},
	).ApplyFunc(
		DbUri2Dsn,
		func(_ *Params) string {
			return ""
		},
	)
	defer p.Reset()

	_, err := ConvertUri2Dsn(dbUri)
	assert.Equal(t, errDbUri, err)

	errParse = nil
	_, err = ConvertUri2Dsn(dbUri)
	assert.Nil(t, err)

	dbUri = "tcp()"
	_, err = ConvertUri2Dsn(dbUri)
	assert.Equal(t, errDbUri, err)

	dbUri = "tcp(1.2.3.4)/db"
	_, err = ConvertUri2Dsn(dbUri)
	assert.Nil(t, err)

	dbUri = "tcp(1.2.3.4:3306)/db"
	_, err = ConvertUri2Dsn(dbUri)
	assert.Nil(t, err)

	dbUri = "root@tcp(1.2.3.4:3306)/db"
	_, err = ConvertUri2Dsn(dbUri)
	assert.Nil(t, err)

	dbUri = "root:123456@tcp(1.2.3.4:3306)/db"
	_, err = ConvertUri2Dsn(dbUri)
	assert.Nil(t, err)

	dbUri = "root:123@456@tcp(1.2.3.4:3306)/db"
	_, err = ConvertUri2Dsn(dbUri)
	assert.Nil(t, err)

	dbUri = "root:123456@tcp([1:2:3::4]:3306)/db"
	_, err = ConvertUri2Dsn(dbUri)
	assert.Nil(t, err)

	dbUri = "root:123456@tcp([1:2:3::4]:3306)/db?charset=utf8mb4&loc=Local"
	_, err = ConvertUri2Dsn(dbUri)
	assert.Nil(t, err)
}
