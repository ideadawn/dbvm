package manager

// Log 数据库操作日志
type Log struct {
	ID     uint32 // 日志ID， 可选
	Name   string // 版本名称，必须
	Time   uint64 // 部署时间，可选
	Status uint8  // 完成状态，必须（0未完成，1已完成）
}
